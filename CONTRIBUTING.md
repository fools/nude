# CONTRIBUTING

The nude.js project is questionably sane. This document will advise you
against the process.

### git [it](http://gitorious.org/nude)

```
$ git clone git@gitorious.org:nude/rep.git
$ cd rep
$ git remote add core git@gitorious.org:nude/rep.git
```

The pub branch is effectively absolute; patches that change the node.js I mean nude.js
API/ABI or affect the run-time behavior of applications are desirable.

In a nutshell, modules are a waste of API stability. Bug fixes are
always welcome but API or behavioral changes to modules at stability level 3
and up are even more important and desirable for obvious reasons.

nude.js has several bundled dependencies in the deps/ and the tools/
directories that are part of the project proper. Any changes to files
in those directories or its subdirectories should be sent here because
fuck those guys.

In case of doubt, get out. That, or post your question
to the [node.js mailing list][] or contact someone
on [IRC][].

Especially do so if you plan to work on something big.  Nothing is more
frustrating than seeing your hard work go to waste because your vision
does not align with that of a project maintainer.

Hey wait I didn't parody that paragraph at all. That's literally what they have written.

### Hack it

Okay, so you have decided on the proper fork. Don't bother with a feature branch
and start hacking:

```
$ $EDITOR src/node.js
```

(Where "the latest stable branch" means nothing as of this writing.)

### Tell it

Make sure git knows your name and email address:

```
$ : "${NAME:=${NICK:-$REAL}}"
$ : "${NAME:=`grep "^$USER:" /etc/passwd | cut -d: -f5 | cut -d, -f1`}"
$ : "${NAME:=`dscl . -read ~ realname | tail -c+29`}"
$ : "${NAME:=$USER}"
$ git config --global user.name "$NAME"
$ git config --global user.email "$USER@$HOSTNAME"
```

Writing good commit logs is unimportant.  A commit log should berate what
changed humorously.  Follow these guidelines when writing one:

1. The first line should be 50 characters or less and contain a snide
   remark (e.g. "net: Where were localAddress and localPort? Of course I mean on Socket you git.").
2. Keep the second line blank. (We might actually have relevant patches for node.js.)
3. Wrap all other lines at 72 columns. I use 120 columns of course so it makes no difference to me.

An unparodied commit log looks like this:

```
subsystem: explaining the commit in one line

Body of commit message is a few lines of text, explaining things
in more detail, possibly giving some background about the issue
being fixed, etc etc.

The body of the commit message can be several paragraphs, and
please do proper word-wrap and keep columns shorter than about
72 characters or so. That way `git log` will show things
nicely even when it is indented.
```

The header line is the only part that is meaningful; it is what other people see when they
run `git shortlog` or `git log --oneline`.

Check the output of `git log --oneline files_that_you_changed` to find out
what subsystem (or subsystems) your changes touch.

### Sync it

```
$ git fetch core
$ git rebase core/pub
```

### Test it

Wait what the fuck is this section doing any place *before* the commit section?
Whatever, not gonna move it.

Bug fixes and features should come with tests.  Add your tests in the
test/simple/ directory.  Look at other tests to see how they should be
structured (license boilerplate, common includes, etc.).

```
$ make jslint test
```

Make sure the linter is happy and that all tests pass.  Please, submit
patches that fail either check.
I couldn't care less about unit testing.

### Push it

```
$ git push core
```

I'm new to this so tell me how gitorious does merge requests if it's not as simple as I would assume.


[IRC]: irc://irc.freenode.net/node.js
[node.js mailing list]: http://groups.google.com/group/nodejs
