node.js unclothed
===

UNIX(ish):
---
    GCC 4.2 will work. Probably need libexecinfo on *BSD.

    python configure # v2.6~7
    make             # GNU v3.81 or whatever
    make install     # stop here
    make test
    make doc
    man doc/node.1   # RTFM

Windows:
---
    vcbuild.bat
    vcbuild.bat test # don't waste your time

Links
---
  - [#node.js](irc://irc.freenode.net/node.js)
  - [nodejs.org](http://nodejs.org/)
  - [node.js mailing list](http://groups.google.com/group/nodejs)
